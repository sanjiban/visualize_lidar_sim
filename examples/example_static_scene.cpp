/* Copyright 2015 Sanjiban Choudhury
 * example_static_scene.cpp
 *
 *  Created on: Feb 27, 2017
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>
#include "visualize_lidar_sim/visualization_utils.h"

namespace vu = visualize_lidar_sim;

int main(int argc, char **argv) {
  ros::init(argc, argv, "example_static_scene");
  ros::NodeHandle n("~");

  ros::Publisher pub_triangle_m = n.advertise<visualization_msgs::Marker>("triangle_list", 0);
  ros::Publisher pub_ellipsoid_ma = n.advertise<visualization_msgs::MarkerArray>("ellipsoid_list", 0);
  ros::Publisher pub_points = n.advertise<pcl::PointCloud<pcl::PointXYZ> >("point_list", 0);

  ros::Duration(1.0).sleep();

  std::string triangle_list_filename, ellipsoid_list_filename, point_list_filename;
  double ellipsoid_scale;
  bool result = true;
  result = result && n.getParam("triangle_list_filename", triangle_list_filename);
  result = result && n.getParam("ellipsoid_list_filename", ellipsoid_list_filename);
  result = result && n.getParam("point_list_filename", point_list_filename);
  result = result && n.getParam("ellipsoid_scale", ellipsoid_scale);

  if (!result) {
    ROS_ERROR_STREAM("Failed to load parameters");
    return EXIT_FAILURE;
  }

  visualization_msgs::Marker triangle_list_marker;
  if (!vu::GetTriangleList(triangle_list_filename, triangle_list_marker)) {
    ROS_ERROR_STREAM("Failed to load triangle_list");
    return EXIT_FAILURE;
  }

  visualization_msgs::MarkerArray ellipsoid_marker_array;
  if (!vu::GetEllipsoidList(ellipsoid_list_filename, ellipsoid_marker_array)) {
    ROS_ERROR_STREAM("Failed to load ellipsoid_list");
    return EXIT_FAILURE;
  }

  vu::InflateEllipsoidList(ellipsoid_marker_array, ellipsoid_scale);

  pcl::PointCloud<pcl::PointXYZ>::Ptr point_msg (new pcl::PointCloud<pcl::PointXYZ>);
  point_msg->header.frame_id = "world";
  if (!vu::GetPointList(point_list_filename, *point_msg)) {
    ROS_ERROR_STREAM("Failed to load point list");
    return EXIT_FAILURE;
  }

  pub_triangle_m.publish(triangle_list_marker);
  pub_ellipsoid_ma.publish(ellipsoid_marker_array);
  pub_points.publish(point_msg);
  ros::Duration(1.0).sleep();
}



