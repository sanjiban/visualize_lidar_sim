/* Copyright 2015 Sanjiban Choudhury
 * scratch.cpp
 *
 *  Created on: Feb 27, 2017
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>
#include "visualization_msgs/MarkerArray.h"

//#define SPHERE
#define TRIANGLE

int main(int argc, char **argv) {
  ros::init(argc, argv, "scratch");
  ros::NodeHandle n("~");

  ros::Publisher pub_sphere_ma = n.advertise<visualization_msgs::MarkerArray>("sphere_array", 0);
  ros::Publisher pub_triangle_m = n.advertise<visualization_msgs::Marker>("triangle_list", 0);

  ros::Duration(1.0).sleep();


  // Lets create ellipses!
#ifdef SPHERE
  visualization_msgs::MarkerArray sphere_ma;
  visualization_msgs::Marker marker;
  marker.header.frame_id = "world";
  marker.type = visualization_msgs::Marker::SPHERE;
  marker.action = visualization_msgs::Marker::ADD;
  marker.ns = "ellipse";
  marker.color.r = 0.0;
  marker.color.g = 1.0;
  marker.color.b = 0.0;

  unsigned int id = 0;
  for (double x = 0; x <= 100; x+=10) {
    for (double y = 0; y <= 100; y+=10) {
      marker.id = id++;

      marker.pose.position.x = x;
      marker.pose.position.y = y;
      marker.pose.position.z = 0;
      marker.pose.orientation.x = 0.0;
      marker.pose.orientation.y = 0.0;
      marker.pose.orientation.z = 0.0;
      marker.pose.orientation.w = 1.0;

      marker.scale.x = 30;
      marker.scale.y = 15;
      marker.scale.z = 10;
      marker.color.a = 0.4; // Don't forget to set the alpha!

      sphere_ma.markers.push_back(marker);
    }
  }
  pub_sphere_ma.publish(sphere_ma);
#endif



#ifdef TRIANGLE
  visualization_msgs::Marker marker;
  marker.header.frame_id = "world";
  marker.type = visualization_msgs::Marker::TRIANGLE_LIST;
  marker.action = visualization_msgs::Marker::ADD;
  marker.ns = "triangle_list";
  marker.pose.position.x = 0;
  marker.pose.position.y = 0;
  marker.pose.position.z = 0;
  marker.pose.orientation.x = 0.0;
  marker.pose.orientation.y = 0.0;
  marker.pose.orientation.z = 0.0;
  marker.pose.orientation.w = 1.0;
  marker.scale.x = 1.0;
  marker.scale.y = 1.0;
  marker.scale.z = 1.0;
  marker.color.r = 1.0;
  marker.color.g = 1.0;
  marker.color.b = 1.0;
  marker.color.a = 1.0;

  for (double x = 0; x <= 100; x+=10) {
    for (double y = 0; y <= 100; y+=10) {
      geometry_msgs::Point pt;
      std_msgs::ColorRGBA col;

      col.r = 153.0/255.0;
      col.g = 73.0/255.0;
      col.b = 0;
      col.a = 0.5;

      pt.x = x; pt.y = y + 15; pt.z = 0;
      marker.points.push_back(pt); marker.colors.push_back(col);

      pt.x = x + 15; pt.y = y; pt.z = 0;
      marker.points.push_back(pt); marker.colors.push_back(col);

      pt.x = x + 15; pt.y = y + 15; pt.z = 0;
      marker.points.push_back(pt); marker.colors.push_back(col);
    }
  }
  pub_triangle_m.publish(marker);
#endif

  ros::Duration(1.0).sleep();

}


