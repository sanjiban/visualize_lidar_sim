/* Copyright 2015 Sanjiban Choudhury
 * visualization_utils.h
 *
 *  Created on: Feb 27, 2017
 *      Author: Sanjiban Choudhury
 */

#ifndef VISUALIZE_LIDAR_SIM_INCLUDE_VISUALIZE_LIDAR_SIM_VISUALIZATION_UTILS_H_
#define VISUALIZE_LIDAR_SIM_INCLUDE_VISUALIZE_LIDAR_SIM_VISUALIZATION_UTILS_H_

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <Eigen/Dense>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>

namespace visualize_lidar_sim {

bool GetTriangleList(const std::string &filename, visualization_msgs::Marker &triangle_list);

bool GetEllipsoidList(const std::string &filename, visualization_msgs::MarkerArray &ellipsoid_list);

bool GetPointList(const std::string &filename, pcl::PointCloud<pcl::PointXYZ> &pcl);


void InflateEllipsoidList(visualization_msgs::MarkerArray &ellipsoid_list, double scale);

void GetEllipseTransform(const Eigen::Matrix3d &input, Eigen::Quaterniond &quat, Eigen::Vector3d &scale);

void MergeTriangleList(visualization_msgs::Marker &original, const visualization_msgs::Marker &addition);

void MergeEllipsoidList(visualization_msgs::MarkerArray &original, const visualization_msgs::MarkerArray &addition);

}


#endif /* VISUALIZE_LIDAR_SIM_INCLUDE_VISUALIZE_LIDAR_SIM_VISUALIZATION_UTILS_H_ */
