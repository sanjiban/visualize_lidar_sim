/* Copyright 2015 Sanjiban Choudhury
 * visualization_utils.cpp
 *
 *  Created on: Feb 27, 2017
 *      Author: Sanjiban Choudhury
 */

#include "visualize_lidar_sim/visualization_utils.h"
#include <iostream>
#include <fstream>      // std::ifstream
#include <Eigen/Dense>
#include <Eigen/SVD>

namespace visualize_lidar_sim {

bool GetTriangleList(const std::string &filename, visualization_msgs::Marker &triangle_list) {
  // open input file
  std::ifstream file(filename);
  if (!file.good())
    return false;

  std::cout << "Reading triangle models from: " << filename << std::endl;

  std::string current_line;
  std::string mode;
  std::vector<std::vector<double> > m_fit_pts;
  std::vector<std::vector<int> > m_triangles;
  std::vector<double> m_hit_prob_vec;

  while(std::getline(file, current_line))
  {
    if (strcmp(current_line.c_str(), "pts") == 0)
    {
      mode = "pts";
      continue;
    }
    if (strcmp(current_line.c_str(), "triangles") == 0)
    {
      mode = "triangles";
      continue;
    }

    std::istringstream iss(current_line);

    if (strcmp(mode.c_str(), "pts") == 0)
    {
      std::vector<double> pt(3,0);
      for(size_t i = 0; i < 3; ++i)
        iss >> pt[i];

      m_fit_pts.push_back(pt);
    }

    if (strcmp(mode.c_str(), "triangles") == 0)
    {
      std::vector<int> triangle(3,0);
      for(size_t i = 0; i < 3; ++i)
        iss >> triangle[i];

      m_triangles.push_back(triangle);

      double hit_prob;
      iss >> hit_prob;
      m_hit_prob_vec.push_back(hit_prob);
    }

  }

  file.close();

  // Fill up a trianlge list and return
  triangle_list.header.frame_id = "world";
  triangle_list.type = visualization_msgs::Marker::TRIANGLE_LIST;
  triangle_list.action = visualization_msgs::Marker::ADD;
  triangle_list.ns = "triangle_list";
  triangle_list.pose.position.x = 0;
  triangle_list.pose.position.y = 0;
  triangle_list.pose.position.z = 0;
  triangle_list.pose.orientation.x = 0.0;
  triangle_list.pose.orientation.y = 0.0;
  triangle_list.pose.orientation.z = 0.0;
  triangle_list.pose.orientation.w = 1.0;
  triangle_list.scale.x = 1.0;
  triangle_list.scale.y = 1.0;
  triangle_list.scale.z = 1.0;
  triangle_list.color.r = 1.0;
  triangle_list.color.g = 1.0;
  triangle_list.color.b = 1.0;
  triangle_list.color.a = 1.0;

  std_msgs::ColorRGBA col;

  col.r = 153.0/255.0;
  col.g = 73.0/255.0;
  col.b = 0;

  for (unsigned int i = 0; i < m_triangles.size(); i++) {
    col.a = m_hit_prob_vec[i];

    geometry_msgs::Point pt;
    for (unsigned int j = 0; j < 3; j++) {
      pt.x = m_fit_pts[m_triangles[i][j]][0];
      pt.y = m_fit_pts[m_triangles[i][j]][1];
      pt.z = m_fit_pts[m_triangles[i][j]][2];

      triangle_list.points.push_back(pt);
      triangle_list.colors.push_back(col);
    }
  }

  return true;
}

bool GetEllipsoidList(const std::string &filename, visualization_msgs::MarkerArray &ellipsoid_list) {
  // open input file
  std::ifstream file(filename);
  if (!file.good())
    return false;

  std::cout << "Reading ellipsoid models from: " << filename << std::endl;

  std::string current_line;
  std::vector< std::vector<double> > m_mu;
  std::vector< Eigen::Quaterniond > m_quat;
  std::vector< Eigen::Vector3d > m_scale;
  std::vector<double> m_hit_prob_vec;

  while(std::getline(file, current_line))
  {
    std::istringstream iss(current_line);

    std::vector<double> mu(3,0);
    for(size_t i = 0; i < 3; ++i)
      iss >> mu[i];

    Eigen::Matrix3d cov_mat = Eigen::Matrix3d::Zero();
    for(size_t i = 0; i < 3; ++i)
      for(size_t j = 0; j < 3; ++j)
        iss >> cov_mat(j,i);

    double hit_prob;
    iss >> hit_prob;

    // Lets transform to get what we want
    Eigen::Quaterniond quat;
    Eigen::Vector3d scale;

    GetEllipseTransform(cov_mat, quat, scale);

    m_mu.push_back(mu);
    m_quat.push_back(quat);
    m_scale.push_back(scale);
    m_hit_prob_vec.push_back(hit_prob);
  }

  file.close();

  // Fill up a marker array and return
  visualization_msgs::Marker marker;
  marker.header.frame_id = "world";
  marker.type = visualization_msgs::Marker::SPHERE;
  marker.action = visualization_msgs::Marker::ADD;
  marker.ns = "ellipse";
  marker.color.r = 0.0;
  marker.color.g = 1.0;
  marker.color.b = 0.0;

  unsigned int id = 0;
  for (unsigned int i = 0; i < m_mu.size(); i++) {
    marker.id = id++;
    marker.pose.position.x = m_mu[i][0];
    marker.pose.position.y = m_mu[i][1];
    marker.pose.position.z = m_mu[i][2];
    marker.pose.orientation.x = m_quat[i].x();
    marker.pose.orientation.y = m_quat[i].y();
    marker.pose.orientation.z = m_quat[i].z();
    marker.pose.orientation.w = m_quat[i].w();

    marker.scale.x = m_scale[i].x();
    marker.scale.y = m_scale[i].y();
    marker.scale.z = m_scale[i].z();
    marker.color.a = std::max(0.0, m_hit_prob_vec[i]);

    ellipsoid_list.markers.push_back(marker);
  }

  return true;
}

bool GetPointList(const std::string &filename, pcl::PointCloud<pcl::PointXYZ> &pcl) {
  std::ifstream input_file(filename);
  if (!input_file.good())
    return false;
  std::cout << "Reading pts from: " << filename << std::endl;

  std::vector<std::vector<double> > pts;
  std::string current_line;
  while(std::getline(input_file,current_line))
  {
    std::istringstream iss(current_line);
    double x, y, z;
    iss >> x;
    iss >> y;
    iss >> z;

    std::vector<double> this_pt;
    this_pt.push_back(x);
    this_pt.push_back(y);
    this_pt.push_back(z);

    pts.push_back(this_pt);
  }

  pcl.width  = pts.size();
  pcl.height = 1;
  pcl.points.resize (pcl.width * pcl.height);

  for (size_t i = 0; i < pcl.points.size (); ++i) {
    pcl.points[i].x = pts[i][0];
    pcl.points[i].y = pts[i][1];
    pcl.points[i].z = pts[i][2];
  }

  return true;
}


void InflateEllipsoidList(visualization_msgs::MarkerArray &ellipsoid_list, double scale) {
  for (auto &it : ellipsoid_list.markers) {
    it.scale.x *= scale;
    it.scale.y *= scale;
    it.scale.z *= scale;
  }
}


void GetEllipseTransform(const Eigen::Matrix3d &input, Eigen::Quaterniond &quat, Eigen::Vector3d &scale) {
  Eigen::Matrix3d levelled_input(input);
  Eigen::JacobiSVD<Eigen::MatrixXd> svd(levelled_input.inverse(), Eigen::ComputeThinU | Eigen::ComputeThinV);
  Eigen::Matrix3d U = svd.matrixU();
  Eigen::Vector3d singular_values = svd.singularValues();

  quat = Eigen::Quaterniond(U);

  // scales
  for(size_t i = 0; i < 3; ++i)
    scale(i) = 1/std::sqrt(singular_values(i));
}

void MergeTriangleList(visualization_msgs::Marker &original, const visualization_msgs::Marker &addition) {
  if (original.points.empty()) {
    original = addition;
    return;
  }

  original.points.insert( original.points.end(), addition.points.begin(), addition.points.end() );
  original.colors.insert( original.colors.end(), addition.colors.begin(), addition.colors.end() );
}

void MergeEllipsoidList(visualization_msgs::MarkerArray &original, const visualization_msgs::MarkerArray &addition) {
  if (original.markers.empty()) {
    original = addition;
    return;
  }

  unsigned int last_id = original.markers.back().id + 1;
  for (auto it : addition.markers) {
    it.id += last_id;
    original.markers.push_back(it);
  }
}

}



